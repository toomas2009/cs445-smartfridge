package Database;

public class Dairy extends FoodItem{
	myDate expDate;
	
	public Dairy(String itemName,int quan,Units unitz,int thresh, myDate outDate){
		super(itemName,quan,unitz,thresh);
		expDate = outDate;
	}
	
	public Dairy(String itemName,int quan,Units unitz,int thresh, int daysLater){
		super(itemName,quan,unitz,thresh);
		myDate date = expDate.calcexpirationDate(daysLater);
		expDate.setDay(date.getDay());
		expDate.setMonth(date.getMonth());
		expDate.setYear(date.getYear());
	}
	
	public void setExpDate(myDate outDate){
		expDate = outDate;
	}
	public void setExpDate(int daysLater){
		myDate date = expDate.calcexpirationDate(daysLater);
		expDate.setDay(date.getDay());
		expDate.setMonth(date.getMonth());
		expDate.setYear(date.getYear());
	}
	
	public myDate getExpDate(){
		return expDate;
	}
	
	public String toString(){
		return super.toString().concat(" and your item expires on: " + expDate.toString());
	}
	
	public Dairy clone(){
		Dairy newCow = new Dairy(getName(),getQuantity(),getUnits(),getThreshold(), expDate);
		return newCow;
	}
	
	public boolean equals(Dairy cow){
		if(super.equals(cow)){
			if(expDate.equals(cow.expDate)){
				return true;
			}
		}
		return false;
	}
}
