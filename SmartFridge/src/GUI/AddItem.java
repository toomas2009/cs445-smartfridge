package GUI;

/**
*
* @author sneha.angel
*/

import Database.*;

import javax.swing.*;


import java.awt.*;
import java.awt.event.*;

@SuppressWarnings("serial")
public class AddItem extends JFrame {
	private java.util.List<FoodItem> fridge;
	private Container contents;
	private JButton Beverage, Dairy, Meat, Produce,
					Other, exitPage;
	
	public AddItem() {
		super("Add Item");
		contents = getContentPane();
		contents.setLayout(new FlowLayout());
		
		// add customizable font
		Font font = new Font("Verdana", Font.BOLD, 13);
		
		// create and customize buttons
		Beverage = new JButton("Beverage");
		Beverage.setFont(font);
		Beverage.setForeground(Color.WHITE);
		Beverage.setBackground(Color.GREEN);
		
		Dairy = new JButton("Dairy");
		Dairy.setFont(font);
		Dairy.setForeground(Color.WHITE);
		Dairy.setBackground(Color.ORANGE);
		
		Meat = new JButton("Meat");
		Meat.setFont(font);
		Meat.setForeground(Color.WHITE);
		Meat.setBackground(Color.MAGENTA);
		
		Produce = new JButton("Produce");
		Produce.setFont(font);
		Produce.setForeground(Color.WHITE);
		Produce.setBackground(Color.CYAN);
		
		Other = new JButton("Other");
		Other.setFont(font);
		Other.setForeground(Color.WHITE);
		Other.setBackground(Color.PINK);
		
		exitPage = new JButton("Exit");
		exitPage.setFont(font);
		exitPage.setForeground(Color.WHITE);
		exitPage.setBackground(Color.RED);
		
		// add buttons to contents
		contents.add(Beverage);
		contents.add(Dairy);
		contents.add(Meat);
		contents.add(Produce);
		contents.add(Other);
		contents.add(exitPage);
		
		// add event handler
		ButtonHandler bh = new ButtonHandler();
		Beverage.addActionListener(bh);
		Dairy.addActionListener(bh);
		Meat.addActionListener(bh);
		Produce.addActionListener(bh);
		Other.addActionListener(bh);
		exitPage.addActionListener(bh);
		
		setSize(225,235);
		setVisible(true);
	}
	
	private class ButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			if(ae.getSource() == exitPage){
				setVisible(false);
			}
			String itemAdd = JOptionPane.showInputDialog("What item " +
					"would you like to add?");
			String addAmount = JOptionPane.showInputDialog("Enter quantity:");
			String units = JOptionPane.showInputDialog("What are the units you use for this item? " +
					"(u,oz,gr,kg,mL,L,lb,floz,c,q,ga,tsp,tbsp,box,crate)");
			String threshold = JOptionPane.showInputDialog("What is the threshold at which you "
					+ "would like to be notified to buy more?");
			int quantity = Integer.parseInt(addAmount);
			int thresh = Integer.parseInt(threshold);
			Units uni = Units.u;
			switch(units.toLowerCase()){
			case"oz":
				uni = Units.oz;
				break;
			case"gr": 
				uni = Units.gr;
				break;
			case"kg":
				uni = Units.kg;
				break;
			case"ml":
				uni = Units.mL;
				break;
			case"l":
				uni = Units.L;
				break;
			case"lb":
				uni = Units.lb;
				break;
			case"floz":
				uni = Units.floz;
				break;
			case"C":
				uni = Units.c;
				break;
			case"q":
				uni = Units.q;
				break;
			case"ga":
				uni = Units.ga;
				break;
			case"tsp":
				uni = Units.tsp;
				break;
			case"tbsp":
				uni = Units.tbsp;
				break;
			case"box":
				uni = Units.box;
				break;
			case"crate":
				uni = Units.crate;
				
			}
			if (ae.getSource() == Beverage){
				String alcoholic = JOptionPane.showInputDialog("Is this Beverage alcoholic?");
				
				boolean alco = alcoholic.toLowerCase().equals("yes");
				Database.Beverage newBev = new Database.Beverage(itemAdd,quantity,uni,thresh,alco);
				fridge.add(newBev);
			}
			else if (ae.getSource() == Dairy)
			{
				String date = JOptionPane.showInputDialog("What is the expiration date of this item?(mm/dd/yyyy)");
				myDate expDate = new myDate(Integer.parseInt(date.substring(1, 2)),Integer.parseInt(date.substring(4,5)),Integer.parseInt(date.substring(7,10)));
				Dairy moo = new Dairy(itemAdd,quantity,uni,thresh,expDate);
				fridge.add(moo);
			}
			else if (ae.getSource() == Meat)
			{
				String type = JOptionPane.showInputDialog("What type of meat is it?");
				String date = JOptionPane.showInputDialog("What is the expiration date of this item?(mm/dd/yyyy)");
				myDate expDate = new myDate(Integer.parseInt(date.substring(1, 2)),Integer.parseInt(date.substring(4,5)),Integer.parseInt(date.substring(7,10)));
				Meat newMeat = new Meat(itemAdd,quantity,uni,thresh,expDate,type);
				fridge.add(newMeat);
			}
			else if (ae.getSource() == Produce)
			{
				String PLU = JOptionPane.showInputDialog("What is it's PLU#");
				Produce farm = new Produce(itemAdd,quantity,uni,thresh,Integer.parseInt(PLU));
				fridge.add(farm);
			}
			else if (ae.getSource() == Other)
			{
				FoodItem foo = new FoodItem(itemAdd,quantity,uni,thresh);
				fridge.add(foo);
			}
		}
	}
	
	public static void main(java.util.List<FoodItem> fridgedige) {
		AddItem newI = new AddItem();
		newI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		newI.fridge = fridgedige;
		return;
	}
}
