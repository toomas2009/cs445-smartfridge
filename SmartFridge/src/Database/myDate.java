package Database;

import java.util.Calendar;

public class myDate {
	private int day;
	private int month;
	private int year;
	
	public myDate(){
		day = Calendar.DAY_OF_MONTH;
		month = Calendar.MONTH;
		year = Calendar.YEAR;
	}
	
	public myDate(int expDay,int expMonth, int expYear){
		day = expDay;
		month = expMonth;
		year = expYear;
	}
	
	public void setDay(int newDay){
		day = newDay;
	}
	public void setMonth(int newMonth){
		month = newMonth;
	}
	public void setYear(int newYear){
		year = newYear;
	}
	
	public int getDay(){
		return day;
	}
	public int getMonth(){
		return month;
	}
	
	public int getYear(){
		return year;
	}
	
	public myDate calcexpirationDate(int daysToExp ){
		int newDay = day + daysToExp;
		myDate expDate = clone();
		if(newDay >= 28){
			while(newDay >= 28){
				if(month == 1){
					expDate.setDay(newDay-28);
					expDate.setMonth(++expDate.month);
					//expDate = new myDate(newDay-28,++month,year);
				}
				else if(newDay == 30 && (month == 3 || month == 5 || month == 8 || month == 10)){
					expDate.setDay( newDay-30);
					expDate.setMonth(++expDate.month);
					//expDate = new myDate(newDay-30,++month,year);
				}
				else{
					expDate.setDay(newDay-31);
					if(month == 11){
						expDate.setMonth(0);
						expDate.setYear(++expDate.year);
					}
					else{
						expDate.setMonth(++expDate.month);
					}
					//expDate = new myDate(newDay-31,++month,year);
				}
			}
		}
		else{
			expDate = new myDate(newDay,month,year);
		}
		return expDate;
	}
	
	public String toString(){
		String monthName;
		String output = " " + day + " " + year;
		switch(month){
		case 0: monthName = "January";break;
		case 1: monthName = "February";break;
		case 2: monthName = "March";break;
		case 3: monthName = "April";break;
		case 4: monthName = "May";break;
		case 5: monthName = "June";break;
		case 6: monthName = "July";break;
		case 7: monthName = "August";break;
		case 8: monthName = "September";break;
		case 9: monthName = "October";break;
		case 10: monthName = "November";break;
		case 11: monthName = "December";break;
		default: monthName = "error";break;
		}
		monthName.concat(output);
		return monthName;
	}
	
	public myDate clone(){
		myDate newDate = new myDate(day,month,year);
		return newDate;
		
	}
	
	public boolean equals(myDate date){
		if(day == date.day){
			if(month == date.month){
				if(year == date.year)
					return true;
			}
		}
		return false;
	}
}
