package Database;

public class Beverage extends FoodItem {
	boolean alcoholic;
	
	public  Beverage(String itemName,int quan,Units unitz,int thresh, boolean alc){
		super(itemName,quan,unitz,thresh);
		alcoholic = alc;
	}
	
	public boolean getAlcoholic(){
		return alcoholic;
	}
	public void setAlcoholic(boolean alc){
		alcoholic = alc;
	}
	
	public String toString(){
		String output = super.toString();
		if(alcoholic){
			output.concat(" and your beverage is alcoholic.");
		}
		else{
			output.concat(" and your beverage is non-alcoholic.");
		}
		return output;
	}
	
	public Beverage clone(){
		Beverage newBev = new Beverage(getName(),getQuantity(),getUnits(),getThreshold(),alcoholic);
		return newBev;
	}
	
	public boolean equals(Beverage roygbev){
		if(super.equals(roygbev)){
			if(alcoholic && roygbev.alcoholic){
				return true;
			}
		}
		return false;
	}
}