package Database;

public class Produce extends FoodItem{
	int PLU;
	
	public Produce(String itemName,int quan,Units unitz,int thresh, int prodNum){
		super(itemName,quan,unitz,thresh);
		PLU = prodNum;
	}
	
	public void setPLU(int prodNum){
		PLU = prodNum;
	}
	
	public int getPLU(){
		return PLU;
	}
	
	public String toString(){
		return super.toString().concat(" and the price look-up code is: " + PLU + ".");
	}
	
	public Produce clone(){
		Produce produ = new Produce(getName(),getQuantity(),getUnits(),getThreshold(), PLU);
		return produ;
	}
	
	public boolean equals(Produce angryOrange){
		if(super.equals(angryOrange)){
			if(PLU == angryOrange.PLU){
				return true;
			}
		}
		return false;
	}
}
