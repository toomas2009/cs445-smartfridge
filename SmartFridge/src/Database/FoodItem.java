package Database;

public class FoodItem {
	private myDate purchaseDate = new myDate();
	private String name = " ";
	private int quantity = 0;
	private Units unit;
	private int threshold = 0;
	

	public FoodItem(String itemName,int quan,Units unitz,int thresh){
		name = itemName;
		quantity = quan;
		unit = unitz;
		threshold = thresh;
	}
	
	public void setName(String title){
		name = title;
	}
	public void setQuantity(int quan){
		quantity = quan;
	}
	public void setUnits(Units uni){
		unit = uni;
	}
	public void setThreshold(int thresh){
		threshold = thresh;
	}
	
	public String getName(){
		return name;
	}
	public int getQuantity(){
		return quantity;
	}
	public Units getUnits(){
		return unit;
	}
	public int getThreshold(){
		return threshold;
	}

	public String toString(){
		String output = "Your: " + name + "was added to the fridge on: " + purchaseDate.toString() + ", can be found on: "
				+ ", and you have: " + quantity + " " + unit + " left";
		return output;
	}
	
	public FoodItem clone(){
		FoodItem nfi = new FoodItem(name,quantity,unit,threshold);
		return nfi;
	}
	
	public boolean equals(FoodItem food){
		if(name.equals(food.name)){
			if(quantity == food.quantity){
				if(unit == food.unit){
					if(purchaseDate.equals(food.purchaseDate)){
						if(threshold == food.threshold){
							return true;
						}
					}
				}
			}
		}
		return false;
	}
}
