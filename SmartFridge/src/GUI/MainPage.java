package GUI;

/**
*
* @author sneha.angel
*/

import smart.fridge.*;

import javax.swing.*;

import Database.FoodItem;

import java.awt.*;
import java.awt.event.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

@SuppressWarnings("serial")
public class MainPage extends JFrame {
	private java.util.List<FoodItem> fridge = new java.util.ArrayList<FoodItem>();
	private Container contents;
	private JButton addItem, takeItem, checkInventory, editAccount,
					editThreshold, exitPage;
	
	public MainPage() {
		super("Main Page");
		contents = getContentPane();
		contents.setLayout(new FlowLayout());
		
		// add customizable font
		Font font = new Font("Verdana", Font.BOLD, 13);
		
		// create and customize buttons
		addItem = new JButton("Add Item");
		addItem.setFont(font);
		addItem.setForeground(Color.WHITE);
		addItem.setBackground(Color.GREEN);
		
		takeItem = new JButton("Take Item");
		takeItem.setFont(font);
		takeItem.setForeground(Color.WHITE);
		takeItem.setBackground(Color.ORANGE);
		
		checkInventory = new JButton("Check Inventory");
		checkInventory.setFont(font);
		checkInventory.setForeground(Color.WHITE);
		checkInventory.setBackground(Color.MAGENTA);
		
		editAccount = new JButton("Edit Account");
		editAccount.setFont(font);
		editAccount.setForeground(Color.WHITE);
		editAccount.setBackground(Color.CYAN);
		
		editThreshold = new JButton("Change Threshold");
		editThreshold.setFont(font);
		editThreshold.setForeground(Color.WHITE);
		editThreshold.setBackground(Color.PINK);
		
		exitPage = new JButton("Exit");
		exitPage.setFont(font);
		exitPage.setForeground(Color.WHITE);
		exitPage.setBackground(Color.RED);
		
		// add buttons to contents
		contents.add(addItem);
		contents.add(takeItem);
		contents.add(checkInventory);
		contents.add(editAccount);
		contents.add(editThreshold);
		contents.add(exitPage);
		
		// add event handler
		ButtonHandler bh = new ButtonHandler();
		addItem.addActionListener(bh);
		takeItem.addActionListener(bh);
		checkInventory.addActionListener(bh);
		editAccount.addActionListener(bh);
		editThreshold.addActionListener(bh);
		exitPage.addActionListener(bh);
		
		setSize(225,235);
		setVisible(true);
	}
	
	private class ButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			if (ae.getSource() == addItem)
			{
				AddItem.main(fridge);
			}
			else if (ae.getSource() == editAccount)
			{
				UserPreferences.main(null);
			}
			else if (ae.getSource() == exitPage){
		    	try{
		    		FileOutputStream fstream = new FileOutputStream("objects");
		    		ObjectOutputStream objSave = new ObjectOutputStream(fstream);
		    		for(int i=0;i<fridge.size();i++){
		    			objSave.writeObject(fridge.get(i));
		    			objSave.flush();
		    		}
		    		objSave.close();
		    	}
		    	catch(IOException ioe){
		    		
		    	}
		    	System.exit(0);
			}
			else{
				String find = JOptionPane.showInputDialog("Which item would you like to get information on?");
				for(int i=0;i<fridge.size();i++){
					if(fridge.get(i).getName().equals(find)){
						if(ae.getSource() == takeItem){
							String amount = JOptionPane.showInputDialog("How many of this item would you like to take?");
							fridge.get(i).setQuantity(fridge.get(i).getQuantity()-Integer.parseInt(amount));
						}
						else if(ae.getSource() == checkInventory){
							System.out.println(fridge.get(i).toString());
						}
						else if(ae.getSource() == editThreshold){
							String newHold = JOptionPane.showInputDialog("What would you like the new threshold to be?");
							fridge.get(i).setThreshold(Integer.parseInt(newHold));
						}
					}
				}
			}
		}
	}
	
	public static void main(String[]args) {
		MainPage mp = new MainPage();
		mp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		return;
	}
}
