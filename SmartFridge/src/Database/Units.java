package Database;

public enum Units {
	u, // basic unit
	oz,gr,kg, // 'dry' units
	mL,L,lb,floz, // 'wet' units
	c,q,ga,tsp,tbsp, // either-or
	box,crate // containers
}
