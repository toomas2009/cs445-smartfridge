package Database;

public class Meat extends FoodItem{
	String type;
	myDate expDate;
	
	public Meat(String itemName,int quan,Units unitz,int thresh, myDate outDate,String descrip){
		super(itemName,quan,unitz,thresh);
		type = descrip;
		expDate = outDate;
	}
	
	public Meat(String itemName,int quan,Units unitz,int thresh, int daysLater,String descrip){
		super(itemName,quan,unitz,thresh);
		type = descrip;
		myDate date = expDate.calcexpirationDate(daysLater);
		expDate.setDay(date.getDay());
		expDate.setMonth(date.getMonth());
		expDate.setYear(date.getYear());
	}
	
	public void setType(String descrip){
		type = descrip;
	}
	public void setExpDate(myDate date){
		expDate.setDay(date.getDay());
		expDate.setMonth(date.getMonth());
		expDate.setYear(date.getYear());
	}
	public void setExpDate(int daysLater){
		myDate date = expDate.calcexpirationDate(daysLater);
		expDate.setDay(date.getDay());
		expDate.setMonth(date.getMonth());
		expDate.setYear(date.getYear());
	}
	
	public String getType(){
		return type;
	}
	public myDate getExpDate(){
		return expDate;
	}
	
	public String toString(){
		return super.toString().concat(" your meat is: " + type + " and it expires on: " + expDate.toString());
	}
	
	public Meat clone(){
		Meat newMeat = new Meat(getName(),getQuantity(),getUnits(),getThreshold(), expDate,type);
		return newMeat;
	}
	
	public boolean equals(Meat tasty){
		if(super.equals(tasty)){
			if(type.equals(tasty.type)){
				if(expDate.equals(tasty.expDate)){
					return true;
				}
			}
		}
		return false;
	}
}
